/**
 * 
 */
package com.divergent.objclasses;

/**
 * @author Akash Poddar
 *Shutdown Hooks are a special construct that allows developers to plug in a piece of code to be executed 
 *when the JVM is shutting down. 
 *This comes in handy in cases where we need to do special clean up operations in case the VM is shutting down.
 */
public class ShutDownHook {

	public static void main(String[] args) {

		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				System.out.println("Shutdown Hook is running !");
			}
		});
		System.out.println("Application Terminating ...");
	}
}
