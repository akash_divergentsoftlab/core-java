/**
 * 
 */
package com.divergent.objclasses;

/**
 * @author Akash Poddar
 *
 */
public class Order_execution {
	static class GFG {
		  
	    GFG(int x)
	    {
	        System.out.println("ONE argument constructor");
	    }
	  
	    GFG()
	    {
	        System.out.println("No  argument constructor");
	    }
	  
	    public static void main(String[] args)
	    {
	        new GFG();
	        new GFG(8);
	    }
	}

}
