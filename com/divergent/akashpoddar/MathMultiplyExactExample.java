/**
 * 
 */
package com.divergent.akashpoddar;

import java.util.Scanner;
/**
 * @author Akash Poddar
 *This example source code demonstrates the use of  
 * multiplyExact() method of Math class
 */
public class MathMultiplyExactExample {

	public static void main(String[] args) {
		// Ask for user input
				System.out.print("Enter 1st value:");

				// use scanner to read the console input
				Scanner scan = new Scanner(System.in);

				// Assign the user to String variable
				String value1 = scan.nextLine();
				
				
				// Ask for user input
				System.out.print("Enter 2nd value:");

				// Assign the user to String variable
				String value2 = scan.nextLine();

				// close the scanner object
				scan.close();

				// convert the string input to int
				int x = Integer.parseInt(value1);
				int y = Integer.parseInt(value2);

				// get the result of multiplyExact()
				int result = Math.multiplyExact(x,y);
				System.out.println("Result of the operation is " + result);


	}

}
