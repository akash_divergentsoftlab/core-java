/**
 * 
 */
package com.divergent.akashpoddar;

/**
 * @author Akash Poddar
 *
 */
public class Var_keyword {

	/**
	 * program that uses "var" keyword for local variables.
	 */
	public static void main(String[] args) {
		
		//int 
		var x = 100; 
		
		//double
		var y = 1.90;
		
		//string
		var z = "Divergent";
		
		//char 
		var p = "p";
		
		//boolean
		var c = false;
		
		
		//type inference is used in var keyword in which it  automatically detects the dataype of the variable
		System.out.println(x);
		System.out.println(y);
		System.out.println(z);
		System.out.println(p);
		System.out.println(c);

	}

}
