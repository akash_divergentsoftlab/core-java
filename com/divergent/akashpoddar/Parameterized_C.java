/**
 * 
 */
package com.divergent.akashpoddar;

/**
 * @author Akash Poddar
 *
 */
public class Parameterized_C {

	/**
	 * Example illustrating Parameterized Constructor:
	 */
	String studentName;
	int studentAge;
	// making constructor 
	public Parameterized_C(String name, int age) {
		studentName = name;
		studentAge = age;
	}
	void display() {
		System.out.println(studentName + " " +studentAge);
	}
	public static void main(String[] args) {
		Parameterized_C myObj = new Parameterized_C("Akash", 23);
		myObj.display();
	}
}
