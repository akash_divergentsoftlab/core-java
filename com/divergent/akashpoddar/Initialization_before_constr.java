/**
 * 
 */
package com.divergent.akashpoddar;

/**
 * @author Akash Poddar
 *
 */
public class Initialization_before_constr {

	/**
	 * Java program to illustrate that final instance variable
	 * should be declared at the time of declaration
	 */
	final int x = 100;
	public static void main(String[] args) {
		Initialization_before_constr t = new Initialization_before_constr();
        System.out.println(t.x);

	}

}
