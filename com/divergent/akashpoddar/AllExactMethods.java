/**
 * 
 */
package com.divergent.akashpoddar;

/**
 * @author Akash Poddar
 *
 */
public class AllExactMethods {

	public static void main(String[] args) {
		int x = 20;
        int y = 30;
        Object z;
        
        z = Math.addExact(x, y);
        System.out.println("addExact: " + x + " + " + y + " = " + z); // adding
        z = Math.subtractExact(x, y);
        System.out.println("subtractExact: " + x + " - " + y + " = " + z); //subtracting
        z = Math.multiplyExact(x, y);
        System.out.println("multiplyExact: " + x + " * " + y + " = " + z); //multiplying
        z = Math.incrementExact(x);
        System.out.println("incrementExact: " + x + " + 1 = " + z); //increment
        z = Math.decrementExact(y);
        System.out.println("decrementExact: " + y + " - 1 = " + z); // decrement
        z = Math.negateExact(x);
        System.out.println("negateExact: " + x + " * -1 = " + z);
	}

}
