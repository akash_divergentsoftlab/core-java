package com.divergent.projects;

import java.util.Scanner;

/**
 * @author Akash Poddar
 *
 */
public class Login {
	public static void main(String[] args) {
		//simple login form using only scan and print functions 
		
		try(Scanner scanner = new Scanner(System.in)){
			//storing the username in the variable by taking input
			System.out.print(" Enter the username => ");
			String userName = scanner.nextLine();
			
			// now storing password 
			System.out.println(" Enter the password => ");
			String passWord = scanner.nextLine();
			
			// setting username and password data at coding
			if ("akash".equals(userName) && "password".equals(passWord)) {
				System.out.println("The user is successfully logged in!!"); //displaying the successful message
			}else {
				System.out.println("Entered username or password is wrong!!"); //failed message
			}
		
		
		}
	}
}
