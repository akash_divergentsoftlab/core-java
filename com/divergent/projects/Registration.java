/**
 * this program lets you create a registration form
 */
package com.divergent.projects;


import java.util.Scanner;

/**
 * @author Akash Poddar
 *
 */
public class Registration {
	public static void main(String[] args) {

		Register register = new Register(); //creating object as it can be later used as a reference in backend for database connection

		// using try to automatically close the scanner
		try (Scanner scanner = new Scanner(System.in)) {

			System.out.println(" Enter the first name => ");
			String firstName = scanner.nextLine(); // taking inputs
			register.setFirstName(firstName); // storing into the object

			System.out.println(" Enter the last name => ");
			String lastName = scanner.nextLine();
			register.setLastName(lastName);

			System.out.println(" Enter the sex => ");
			String sex = scanner.nextLine();
			register.setSex(sex);

			System.out.println(" Enter the age => ");
			String age = scanner.nextLine();
			register.setAge(age);

			System.out.println(" Enter the complaint => ");
			String complaint = scanner.nextLine();
			register.setComplaint(complaint);

			System.out.println(" Enter the email  => ");
			String emailId = scanner.nextLine();
			register.setEmailId(emailId);

			System.out.println(" Enter the password => ");
			String passWord = scanner.nextLine();
			register.setPassWord(passWord);

			System.out.println(" Enter the phoneno => ");
			long phoneNo = scanner.nextLong();
			register.setPhoneNo(phoneNo);
			
			printRegisteredData(register);
		}
	}
	private static void printRegisteredData(Register register) {
		System.out.println("The Personal Details are as follows:");
		System.out.println(register.getFirstName());
		System.out.println(register.getLastName());
		System.out.println(register.getSex());
		System.out.println(register.getAge());
		System.out.println(register.getComplaint());
		System.out.println(register.getEmailId());
		System.out.println(register.getPassWord());
		System.out.println(register.getPhoneNo());
		
	}
}

class Register {
	// these are the some personal details of the patient
	private String firstName;
	private String lastName;
	private String sex;
	private String age;
	private String complaint;
	private String emailId;
	private String passWord;
	private long phoneNo;

	// creating getter and setter for accessing the private variables
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getComplaint() {
		return complaint;
	}

	public void setComplaint(String complaint) {
		this.complaint = complaint;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	public long getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(long phoneNo) {
		this.phoneNo = phoneNo;
	}

}
